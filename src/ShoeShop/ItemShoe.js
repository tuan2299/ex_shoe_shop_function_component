import React from "react";
import { Card, Button } from "antd";
const { Meta } = Card;

function ItemShoe({ shoe, handleAddtoCart }) {
  return (
    <div className="col-4 mb-5">
      <Card
        hoverable
        style={{ width: "100%" }}
        cover={
          <img
            style={{ width: "100%", height: "100%" }}
            alt="example"
            src={shoe.image}
          />
        }>
        <Meta title={shoe.name} description={shoe.price} />
        <Button
          onClick={() => {
            handleAddtoCart(shoe);
          }}
          type="primary mt-10">
          Add to Cart
        </Button>
      </Card>
    </div>
  );
}

export default ItemShoe;
