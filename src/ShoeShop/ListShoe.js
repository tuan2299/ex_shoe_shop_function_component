import React from "react";
import ItemShoe from "./ItemShoe";

function ListShoe({ listShoe, handleAddtoCart }) {
  return (
    <div className="container p-5">
      <h2 className="text-center">List Shoe</h2>
      <div className="row mt-10">
        {listShoe.map((shoe, index) => {
          return (
            <ItemShoe
              key={index}
              shoe={shoe}
              handleAddtoCart={handleAddtoCart}
            />
          );
        })}
      </div>
    </div>
  );
}

export default ListShoe;
